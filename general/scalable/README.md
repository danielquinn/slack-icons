# Scalable Icons

Rasterised icons don't scale well and you can't use them in
diagrams either unless you're cool with pixelation.  So I'm working
on growing this list.

|          ![0](0.svg)          |          ![1](1.svg)          |          ![2](2.svg)          |      ![3](3.svg)      |
|:-----------------------------:|:-----------------------------:|:-----------------------------:|:---------------------:|
|               0               |               1               |               2               |           3           |
|          ![4](4.svg)          |          ![5](5.svg)          |          ![6](6.svg)          |      ![7](7.svg)      |
|               4               |               5               |               6               |           7           |
|          ![8](8.svg)          |          ![9](9.svg)          |  ![cockroach](cockroach.svg)  | ![golang](golang.svg) |
|               8               |               9               |           cockroach           |        golang         |
| ![javascript](javascript.svg) |      ![kafka](kafka.svg)      | ![kubernetes](kubernetes.svg) |  ![lemmy](lemmy.svg)  |
|          javascript           |             kafka             |          kubernetes           |         lemmy         |
|     ![nodejs](nodejs.svg)     | ![postgresql](postgresql.svg) |     ![python](python.svg)     |  ![react](react.svg)  |
|            nodejs             |          postgresql           |            python             |         react         |
