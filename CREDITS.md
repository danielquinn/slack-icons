# Credits

I've collected these from all over the place, most of which had no attribution
available that I could find, but where I can get it, I'll include them here:

* `django-pony-rocket.png`: [Sangeeta Jadoonanan](https://djangostickers.com/)
* `django-pony-rockstar.png`: [Sangeeta Jadoonanan](https://djangostickers.com/)
* `django.png`: [Fosstodon](https://emojos.in/fosstodon.org)
* `duckduckgo.png`: [Fosstodon](https://emojos.in/fosstodon.org)
* `freebsd.png`: [Fosstodon](https://emojos.in/fosstodon.org)
* `gimp.png`: [Fosstodon](https://emojos.in/fosstodon.org)
* `microsoft.png`: [Fosstodon](https://emojos.in/fosstodon.org)
* `opensource.png`: [Fosstodon](https://emojos.in/fosstodon.org)
* `raspberry-pi.png`: [Fosstodon](https://emojos.in/fosstodon.org)
* `ripe-ncc.png`: [The RIPE NCC website](https://ripe.net/)
* `steam-deck.png`: [Fosstodon](https://emojos.in/fosstodon.org)
* `terminal.png`: [Fosstodon](https://emojos.in/fosstodon.org)
* `windows.png`: [Fosstodon](https://emojos.in/fosstodon.org)
