# ❤️ Eurovision Flags!

These are all pulled from [this Wikipedia page](https://commons.wikimedia.org/wiki/Flag_heart_symbols_of_Eurovision),
but this list is a subset (basically the first few sections) as I
got tired of copying them down and renaming them to the proper ISO
codes by hand.  If your country isn't listed and you'd like it to
be, now you know where to find it, and you can issue a merge
request to add it 😉

| ![ad](ad.svg) |     ![al](al.svg)     |     ![am](am.svg)     | ![at](at.svg) | ![au](au.svg) | ![az](az.svg) | ![ba](ba.svg) |
|:-------------:|:---------------------:|:---------------------:|:-------------:|:-------------:|:-------------:|:-------------:|
|      ad       |          al           |          am           |      at       |      au       |      az       |      ba       |
| ![be](be.svg) |     ![bg](bg.svg)     |     ![by](by.svg)     | ![ca](ca.svg) | ![cy](cy.svg) | ![cz](cz.svg) | ![de](de.svg) |
|      be       |          bg           |          by           |      ca       |      cy       |      cz       |      de       |
| ![dk](dk.svg) |     ![dz](dz.svg)     |     ![ee](ee.svg)     | ![eg](eg.svg) | ![el](el.svg) | ![es](es.svg) | ![fi](fi.svg) |
|      dk       |          dz           |          ee           |      eg       |      el       |      es       |      fi       |
| ![fr](fr.svg) | ![gb-sct](gb-sct.svg) | ![gb-wls](gb-wls.svg) | ![gb](gb.svg) | ![ge](ge.svg) | ![hr](hr.svg) | ![hu](hu.svg) |
|      fr       |        gb-sct         |        gb-wls         |      gb       |      ge       |      hr       |      hu       |
| ![ie](ie.svg) |     ![is](is.svg)     |     ![it](it.svg)     | ![jo](jo.svg) | ![kz](kz.svg) | ![lb](lb.svg) | ![lt](lt.svg) |
|      ie       |          is           |          it           |      jo       |      kz       |      lb       |      lt       |
| ![lu](lu.svg) |     ![lv](lv.svg)     |     ![ly](ly.svg)     | ![ma](ma.svg) | ![mc](mc.svg) | ![md](md.svg) | ![me](me.svg) |
|      lu       |          lv           |          ly           |      ma       |      mc       |      md       |      me       |
| ![mk](mk.svg) |     ![mt](mt.svg)     |     ![nl](nl.svg)     | ![no](no.svg) | ![pl](pl.svg) | ![pt](pt.svg) | ![ro](ro.svg) |
|      mk       |          mt           |          nl           |      no       |      pl       |      pt       |      ro       |
| ![rs](rs.svg) |     ![ru](ru.svg)     |     ![se](se.svg)     | ![si](si.svg) | ![sk](sk.svg) | ![sm](sm.svg) | ![tn](tn.svg) |
|      rs       |          ru           |          se           |      si       |      sk       |      sm       |      tn       |
| ![va](va.svg) |     ![xk](xk.svg)     |                       |               |               |               |               |
|      va       |          xk           |                       |               |               |               |               |
