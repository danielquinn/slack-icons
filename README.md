# Slack Icons
![Stop killing Palestinian civilians](https://img.shields.io/badge/It%27s%20Genocide-%F0%9F%87%B5%F0%9F%87%B8%20Tech_For_Palestine-D83838?labelColor=01B861&color=D83838&link=https%3A%2F%2Ftechforpalestine.org%2Flearn-more)

A collection of my favourite icons for communication on Slack.

Note that I make no claim to copyright on *any* of these images.  I've simply
collected them from the various Slack channels I've been in over the years and
created a bunch from stuff I've found online when the conversations required
it.

* [The main collection is here](general/bitmap/README.md).  These are all
  rasterised images (png and gif) that scale down just fine for use in Slack.
* [There's also a small collection of scalable images here](general/scalable/README.md)
  (svg) that I'm trying to grow over time.  They're no good for Slack, but I
  like them anyway.
* Finally, I've collected a whole bunch of [Eurovision flags](eurovision/scalable/README.md)
  in SVG format because I love Eurovision and this is my repo so I can do what
  I want with it 😛
